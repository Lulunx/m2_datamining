Moteur de rechere Project
-------------------------

Pour lancer l'application suivre les étapes suivantes :

Pour le serveur :
1. cd ./server/
2. npm install
3. npm start
(Vérifier que l'adresse de SolR est la bonne dans server/index.js line 9)

Pour le client :
1. cd ./prod/
2. npm install serve
3. serve -s dist
4. Ces 3 commandes permettent de servir sur un serveur le dossier dist, il est également tout à fait envisageable de simplement
    Servir le dossier dist, mais il faut que le serveur node soit accessible en localhost ( sur le port 8085 )

Pour SolR :
1. Installer SolR sur le serveur
2. Aller dans le dossier de SolR cd .../solr-x.x.x/
3. Démarrer le serveur : ./bin/solr start
4. Aller sur l'interface de gestion de SolR ( localhost:8983 )
5. Dans le menu collection créer une nouvelle collection nommé restaurants avec config set _default
6. Dézipper le dossier restaurantsTripadvisor.zip
7. Ajouter les restaurants dans SolR : ./bin/post -c restaurants ./restaurantsTripadvisor/* 