let express = require('express');
let app = express();
let got = require('got');
let cors = require('cors');
let fs = require('fs');

app.use(cors());

let solrAddress = "http://localhost:8983/solr/restaurants/";

function findByID(data, id) {
    let obj = false;
    for (let key = 0; key < data.length; key++) {
        if (data[key].id == id) {
            return data[key];

        }
        else if (data[key].hasOwnProperty("children")) {
            obj = findByID(data[key].children, id)
            if (obj != false) {
                return obj;
            }
        }
    }
    return obj;
}

function prepareRq(data) {
    let string = "cuisines:'" + data.name + "'";
    if (data.hasOwnProperty("children")) {
        for (let child in data.children) {
            string += " OR " + prepareRq(data.children[child])
        }
    }

    return string;
}

app.get('/id', function (req, res) {
    got(solrAddress + "select?q=id:" + req.query.q
        , { json: true }).then(response => {
            res.send(response.body.response.docs);
        }).catch(error => {
            console.log("Erreur /id", error);
        });
})

app.get('/getCategories', function (req, res) {
    fs.readFile("./taxonomy.json",
        function (err, data) {
            res.end(data);
        });
});


app.get('/category', function (req, res) {
    data = JSON.parse(fs.readFileSync('./taxonomy.json', 'utf8'));
    if (req.query.q !== undefined) {
        let category = findByID(data, req.query.q);
        let rq = prepareRq(category);
        got(encodeURI(solrAddress + "select?q=" + rq + "&start=" + (req.query.p - 1) * 10 + "&rows=10")
            , { json: true }).then(response => {
                res.send(response.body.response);
            }).catch(error => {
                console.log("Erreur /category", error);
            });
    }
    else {
        res.send([]);
    }
})

app.get('/test', function (req, res) {

    got(encodeURI(solrAddress +
        "select?q=address:" + req.query.q +
        "* OR nom:" + req.query.q +
        "* OR prestations:" + req.query.q +
        "* OR cuisines:" + req.query.q + "*&start=" + (req.query.p - 1) * 10 + "&rows=10")
        , { json: true }).then(response => {
            if (response.body.response.numFound == 0) {
                got(encodeURI(solrAddress +
                    "spell?spellcheck=true&spellcheck.build=true&spellcheck.q=address:" + req.query.q +
                    "* OR nom:" + req.query.q +
                    "* OR prestations:" + req.query.q +
                    "* OR cuisines:" + req.query.q + "*&start=" + (req.query.p - 1) * 10 + "&rows=10")
                    , { json: true }).then(response => {
                        res.send(response.body.response);
                    }).catch(error => {
                        console.log("Erreur get /*", error);
                    });
            }
            else {
                res.send(response.body.response);
            }
        }).catch(error => {
            console.log("Erreur get /*", error);
        });
})

app.get('/*', function (req, res) {

    got(encodeURI(solrAddress +
        "select?q=address:" + req.query.q +
        "* OR nom:" + req.query.q +
        "* OR prestations:" + req.query.q +
        "* OR cuisines:" + req.query.q + "*&start=" + (req.query.p - 1) * 10 + "&rows=10")
        , { json: true }).then(response => {
            res.send(response.body.response);
        }).catch(error => {
            console.log("Erreur get /*", error);
        });
})

app.listen(8085, function () {
    console.log('Listening on port 8085!')
})