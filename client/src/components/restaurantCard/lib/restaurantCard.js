export default {
    data() {
        return {
        }
    },
    computed: {

    },
    methods:{
        goTo:function()
        {
            this.$router.push({ name: "restaurant", query:{id:this.item.id} });
        }
    },
    props: {
        item: {
            type: Object,
            default: function(){
                return {
                    address: "Adresse Inconnue",
                    budget: "Inconnu",
                    cuisines: ["Inconnu"],
                    id: "-1",
                    nom: "Inconnu",
                    phone: "Inconnu",
                    prestations: ["Inconnu"],
                    note: "Inconnu"
                }
            }
        }
    },
    filters: {
        commaNote: function (value) {
          if (!value)
          {
              return ''
          }
          value = value.toString()
          return value.charAt(0) + "," + value.charAt(1)
        }
      }
}