export default {
    data() {
        return {
            selected:5
        }
    },
    computed: {

    },
    methods: {
        onUpdate(selection) {
            this.$router.push({ name: "query", query:{t:selection[0]} });
            this.$router.go()
        },
    },
    props: {
        items: {
            type: Array,
            default: function(){
                return [
                    {
                        "name": "Internationale",
                        "id": 1,
                    }
                ]
            }
        }
    }
}