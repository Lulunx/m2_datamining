export default {
    name: "search",
    data(vm) {
      return {
        searchText: ""
      };
    },
    components: {},
    methods: {
      err: function() {
        this.$store.commit("updateSnackMessage", "Entrez une requête !");
        this.$store.commit("setSnackColor", "error");
        this.$store.commit("showSnack", true);
      },
      search: function(txt) {
        //to="query"
        if (this.searchText == "") {
          this.err();
        } else {
          this.$router.push({ name: "query", query:{q:this.searchText} });
          this.$router.go()

        }
      }
    },
    props:{
        isButton: {
            type:Boolean,
            default: true
        }
    },
    mounted:function(){
        if(this.$route.query.q){
            this.searchText = this.$route.query.q;
        }
    }
  };