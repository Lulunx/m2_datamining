export default {
    data () {
      return {
        
      }
    },
    computed: {
        show :{
            get(){
                return this.$store.state.snackShow;
            },
            set(){
                this.$store.commit("showSnack", false);
            }
        },
        message(){
            return this.$store.state.snackMessage;
        },
        color(){
            return this.$store.state.snackColor;
        }
         
    }
  }