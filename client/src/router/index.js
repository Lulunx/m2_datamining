import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home/Home.vue'
import About from '../views/About/About.vue'
import Query from '../views/Query/Query.vue'
import Restaurant from '../views/Restaurant/Restaurant.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component: About
  }, 
  {
    path: '/query',
    name: 'query',
    component: Query
  },
  {
    path: '/restaurant',
    name: 'restaurant',
    component: Restaurant
  }
]

const router = new VueRouter({
  routes
})

export default router
