import restaurantCard from '../../../components/restaurantCard/restaurantCard';
import taxonomy from '../../../components/taxonomy/taxonomy';
import search from '../../../components/search/search';
import ClipLoader from 'vue-spinner/src/ClipLoader.vue';


export default {
	name: "home",
	data(vm) {
		return {
			restaurants: [],
			categories: [],
			isLoading: true,
			page: null,
			nbPage: 1
		};
	},
	components: {
		restaurantCard,
		taxonomy,
		search,
		ClipLoader
	},
	watch: {
		page: {
			handler: function (val, oldVal) {
				if(oldVal != null)
				{
					this.changePage(val);
				}
			},
		}
	},
	methods: {
		loadCategories: function () {
			let self = this;
			fetch(this.$store.state.serverURL+"/getCategories").then(function (res) {
				res.json().then(function (res) {
					self.categories = res;
				})
			})
		},
		changePage: function (newPage) {
			if (this.$route.query.q) {
				this.$router.push({ name: "query", query: { q: this.$route.query.q, page: newPage } });
			}
			else {
				this.$router.push({ name: "query", query: { t: this.$route.query.t, page: newPage } });
			}
			this.$router.go()
		},
		search: function () {
			let self = this;
			if (this.$route.query.page) {
				this.page = parseInt(this.$route.query.page);
			}
			else {
				this.page = 1
			}
			if (this.$route.query.q) {
				fetch(this.$store.state.serverURL+"?q=" + this.$route.query.q+"&p="+this.page).then(function (res) {
					res.json().then(function (res) {
						self.restaurants = res.docs;
						self.nbPage = Math.ceil(res.numFound / 10);
						self.isLoading = false;
					})
				})
			}
			else if (this.$route.query.t) {
				fetch(this.$store.state.serverURL+"/category?q=" + this.$route.query.t+"&p="+this.page).then(function (res) {
					res.json().then(function (res) {
						self.restaurants = res.docs;
						self.nbPage = Math.ceil(res.numFound / 10);
						self.isLoading = false;

					})
				})
			}
		}
	},
	mounted: function () {
		this.search();
		this.loadCategories();
	}
};