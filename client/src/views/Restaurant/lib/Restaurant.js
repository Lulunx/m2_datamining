export default {
  name: "home",
  data(vm) {
    return {
      restaurant: {
        
      }
    };
  },
  components: {
  },
  methods: {
    search: function () {
      let self = this;
        fetch(this.$store.state.serverURL+"/id?q="+this.$route.query.id).then(function (res) {
          res.json().then(function (res) {
            self.restaurant = res[0];
          })
        })
    },
    goTo:function(){
      window.open("https://maps.google.fr/?q="+this.restaurant.address[0], "_blank");    
    }
  },
  mounted: function () {
    this.search();
  },
  filters: {
      commaNote: function (value) {
        if (!value)
        {
            return 'Inconnu'
        }
        value = value[0].toString()
        return value.charAt(0) + "," + value.charAt(1) + " / 5"
      },
      inconnu: function (value) {
        if (!value)
        {
            return 'Inconnu'
        }
        return value[0]
      }
    }
};